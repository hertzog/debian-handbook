#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2022-07-30 18:23+0200\n"
"PO-Revision-Date: 2013-11-26 15:52+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: None\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Future"
msgstr ""

msgid "Improvements"
msgstr ""

msgid "Opinions"
msgstr ""

msgid "Conclusion: Debian's Future"
msgstr ""

msgid "The story of Falcot Corp ends with this last chapter; but Debian lives on, and the future will certainly bring many interesting surprises."
msgstr ""

msgid "Upcoming Developments"
msgstr ""

msgid "Now that Debian version 11 is out, the developers are already busy working on the next version, codenamed <emphasis role=\"distribution\">Bookworm</emphasis>…"
msgstr ""

msgid "There is no official list of all planned changes, and Debian never makes promises relating to technical goals of the coming versions. However, a few development trends and discussion topics can already be noted, and we can try to guess what might happen (or not). Some of the expected changes are documented in the Debian 11 release notes: <ulink type=\"block\" url=\"https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#deprecated-components\" />"
msgstr ""

msgid "Beyond usual deprecation of some software components, it is worth pointing out that Debian is in the process of switching to what is known as a <emphasis>merged-usr filesystem</emphasis>: in this scheme <literal>/bin</literal>, <literal>/sbin</literal> and <literal>/lib</literal> are symlinks pointing to the corresponding directories in <literal>/usr</literal>. This improves compatibility between all Unix systems, makes us closer of having all Debian-supplied files in a single top-level directory that can be easily protected, snapshotted or shared. You can learn more about the benefits here: <ulink type=\"block\" url=\"https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/\" />"
msgstr ""

msgid "This important change is not without creating issues: <command>dpkg</command> will have to learn about those aliased directories, but the <command>dpkg</command> maintainer doesn't like the technical solution deployed by Debian and hasn't yet made the required changes. The Debian technical committee's help has been requested multiple times already. Their last decision can be found here: <ulink type=\"block\" url=\"https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=994388#110\" />"
msgstr ""

msgid "<primary><command>apt-key</command></primary>"
msgstr ""

msgid "<command>apt-key</command> will become obsolete. The key management for third party repositories should only rely on keys found in <filename>/etc/apt/trusted.gpg.d</filename> or configured via <literal>Signed-By</literal> as described in <xref linkend=\"sect.package-authentication\" />."
msgstr ""

msgid "<primary><command>locate</command></primary>"
msgstr ""

msgid "For some tasks the default software solution will change. As an example: <command>plocate</command> might be a faster and smaller replacement for <command>mlocate</command>. <emphasis>systemd</emphasis> will continue to add new features that help to standardize the boot process and system management, allowing us to get rid of some other software in the base system."
msgstr ""

msgid "Of course all the main software suites will have a major release. The latest version of the various desktops will bring better usability and new features."
msgstr ""

msgid "The default permission of home directories will be more restrictive, allowing only the user to access their files."
msgstr ""

msgid "Developments which already began will continue: Improve build reproducibility and security for example. With the widespread use of continuous integration and the growth of the archive (and of the biggest packages!), the constraints on release architectures will be harder to meet and architectures will be dropped."
msgstr ""

msgid "Debian's Future"
msgstr ""

msgid "In addition to these internal developments, one can reasonably expect new Debian-based distributions to come to light, as many tools keep simplifying this task. New specialized subprojects will also be started, in order to widen Debian's reach to new horizons."
msgstr ""

msgid "The Debian user community will increase, and new contributors will join the project… including, maybe, you!"
msgstr ""

msgid "There are recurring discussions about how the software ecosystem is evolving, towards applications shipped within containers, where Debian packages have no added value, or with language-specific package managers (e.g. <command>pip</command> for Python, <command>npm</command> for JavaScript, etc.), which are rendering <command>dpkg</command> and <command>apt</command> obsolete. Facing those threats, I am convinced that Debian developers will find ways to embrace those changes and to continue to provide value to users."
msgstr ""

msgid "In spite of its old age and its respectable size, Debian keeps on growing in all kinds of (sometimes unexpected) directions. Contributors are teeming with ideas, and discussions on development mailing lists, even when they look like bickerings, keep increasing the momentum. Debian is sometimes compared to a black hole, of such density that any new free software project is attracted."
msgstr ""

msgid "Beyond the apparent satisfaction of most Debian users, a deep trend is becoming more and more indisputable: people (and companies!) are increasingly realizing that collaborating, rather than working alone in their corner, leads to better results for everyone. The number of commercial companies relying on Debian is larger every year."
msgstr ""

msgid "The Debian project is therefore not threatened by extinction…"
msgstr ""

msgid "Future of this Book"
msgstr ""

msgid "We would like this book to evolve in the spirit of free software. We therefore welcome contributions, remarks, suggestions, and criticism. Please direct them to Raphaël (<email>hertzog@debian.org</email>) or Roland (<email>lolando@debian.org</email>). For actionable feedback, feel free to open bug reports against the <literal>debian-handbook</literal> Debian package. The website will be used to gather all information relevant to its evolution, and you will find there information on how to contribute, in particular if you want to translate this book to make it available to an even larger public than today. <ulink type=\"block\" url=\"https://debian-handbook.info/\" /> <ulink type=\"block\" url=\"https://bugs.debian.org/src:debian-handbook\" />"
msgstr ""

msgid "We tried to integrate most of what our experience with Debian taught us, so that anyone can use this distribution and take the best advantage of it as soon as possible. We hope this book contributes to making Debian less confusing and more popular, and we welcome publicity around it!"
msgstr ""

msgid "We would like to conclude on a personal note. Writing (and translating) this book took a considerable amount of time out of our usual professional activity. Since we are both freelance consultants, any new source of income grants us the freedom to spend more time improving Debian; we hope this book to be successful and to contribute to this. In the meantime, feel free to retain our services! <ulink type=\"block\" url=\"https://www.freexian.com\" /> <ulink type=\"block\" url=\"https://www.gnurandal.com\" />"
msgstr ""

msgid "See you soon!"
msgstr ""
